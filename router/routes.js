//initialize express router
const express = require('express')
let router = express.Router();

//set default API response
router.get('/', function(req, res) {
    res.json({
        status: '-',
        message: 'Default Page'
    });
});


//Import Meta Controller
var metaController = require('../control/metaController');
var configController = require('../control/configController');

// Meta routes
router.route('/meta')
    .get(metaController.index)
    .post(metaController.add);

router.route('/meta-bulk')
    .post(metaController.addBulk);

// Meta beamline routes
router.route('/beamline/:beamline')
    .get(metaController.viewBeamline);

// Meta individual record  with id
router.route('/meta/:meta_id')
    .get(metaController.view)
    .patch(metaController.update)
    .put(metaController.update)
    .delete(metaController.delete);

// Config routes
router.route('/config')
    .get(configController.index)
    .post(configController.add);

// Config beamline routes
router.route('/config/beamline/:beamline')
    .get(configController.viewBeamlineConfig)
    .put(configController.updateBeamline)
    .patch(configController.updateBeamline);

// Config for active
router.route('/config/active/:beamline')
    .get(configController.viewMetas)
    .put(configController.updateActive)
    .patch(configController.updateActive);


//Export API routes
module.exports =  {
    router: router
}
