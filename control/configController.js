//Import Config and Meta Model
Config = require('../model/configModel');
Meta = require('../model/metaModel');

const validateAuth = require('../utils/validateAuth');

//For index
exports.index = function (req, res) {
    Config.get(function (err, config) {
        if (err){
            const response = {
                status: "error",
                message: "Couldn't find records " + err
            };
            console.log(response)
            res.status(404).send(response);

        }
        else{
            const response = {
                status: "success",
                message: "Fetched configdata",
                data: config
            };
            res.send(response);
        }
    });
};

//For creating new config record
exports.add = async function (req, res) {
    const valid = await validateAuth(req, res)
    if(valid){
        var skip = false;
        var config = new Config();

        //populating config fields
        req, res, skip = await validateForm(req, res)
        if(!skip){
            config.Beamline = req.body.Beamline
            config.Configuration = req.body.Configuration
            config.ActiveConfig = req.body.ActiveConfig

            //Save and check error
            config.save(function (err) {
                if (err){
                    const response = {
                        status: "error",
                        message: "Couldn't save record " + err
                    };
                    console.log(response)
                    res.status(500).send(response);
                }
                else{
                    const response = {
                        status: "success",
                        message: "Configdata has been successfully added",
                        data: config
                    };
                    console.log(response)
                    res.send(response);
                }
            });
        }
    }
};

// View ConfigData
exports.view = function (req, res) {
    Config.findById(req.params.config_id, function (err, config) {
        if (err){
            const response = {
                status: "error",
                message: "Couldn't find record " + err
            };
            console.log(response)
            res.status(404).send(response);
        }
        else{
            const response = {
                status: "success",
                message: 'Scientific configdata record',
                data: config
            };
            res.send(response);
        }

    });
};

// View Meta records for config
exports.viewMetas = async function (req, res) {
    Config.find({ Beamline: req.params.beamline}, async function (err, config) {
        if (err){
            const response = {
                status: "error",
                message: "Couldn't find record " + err
            };
            console.log(response)
            res.status(404).send(response);
        }
        else{
            var metas = []
            var active = ""
            try{
                active = config[0]['ActiveConfig']
            } catch(err) {
                active = "All"
                console.log("No configuration listed for this beamline: " + err)
            }
            if (active === "All"){
                Meta.find({ Beamline: req.params.beamline}, function (err, meta) {
                    if (err){
                        const response = {
                            status: "error",
                            message: "Couldn't find record " + err
                        };
                        console.log(response)
                        res.status(404).send(response);
                    }
                    else{
                        const response = {
                            status: "success",
                            message: 'Scientific metadata beamline records for ' + req.params.beamline,
                            data: meta
                        };
                        res.send(response);
                    }
                });
            }else{
                var config_list = config[0]['Configuration'][active]
                for(const id of config_list){
                    var meta = await Meta.findById(id)
                    metas.push(meta)
                }
                const response = {
                    status: "success",
                    message: 'Scientific metadata beamline records for ' + req.params.beamline,
                    data: metas
                };
                res.send(response);
            }
        }

    });
};

// View Specific ConfigData
exports.viewBeamlineConfig = function (req, res) {
    Config.find({ Beamline: req.params.beamline}, function (err, config) {
        if (err){
            const response = {
                status: "error",
                message: "Couldn't find record " + err
            };
            console.log(response)
            res.status(404).send(response);
        }
        else{
            var configuration = {}
            if(config.length < 1){
                //if no configuration found, add one
                configuration = new Config();
                configuration.Beamline = req.params.beamline
            }else{
                configuration = config[0]
            }
            const response = {
                status: "success",
                message: 'Scientific configdata beamline records for ' + req.params.beamline,
                data: configuration
            };
            res.send(response);
        }
    });
};

// Update ConfigData
exports.updateBeamline = async function (req, res) {
    const valid = await validateAuth(req, res)
    if(valid){
        Config.find({ Beamline: req.params.beamline}, async function (err, config) {
            if (err){
                const response = {
                    status: "error",
                    message: "Couldn't find record " + err
                };
                console.log(response)
                res.status(404).send(response);
            }
            var configuration = {}
            if(config.length < 1){
                //if no configuration found, add one
                configuration = new Config();
                configuration.Beamline = req.params.beamline
                configuration.Configuration = {}
                configuration.ActiveConfig = "All"
            }else{
                configuration = config[0]
            }
            // config is an array, therefore the first element is beamline record
            if('Configuration' in req.body){
                configuration.Configuration = req.body.Configuration
                if (!(configuration.ActiveConfig in configuration.Configuration) && configuration.ActiveConfig !== "All"){
                    const response = {
                        status: "error",
                        message: "Can't delete active configuration"
                    };
                    res.status(405).send(response);
                }else{
                    //save and check errors
                    configuration.save(function (err) {
                        if (err){
                            const response = {
                                status: "error",
                                message: "Couldn't save record " + err
                            };
                            console.log(response)
                            res.status(500).send(response)
                        }
                        else{
                            const response = {
                                status: "success",
                                message: "Configuration has been successfully updated",
                                data: configuration
                            };
                            console.log(response)
                            res.send(response);
                        }
                    });
                }
            }else{
                const response = {
                    status: "error",
                    message: "No valid configuration found"
                };
                console.log(response)
                res.status(404).send(response);
            }

        });
    }
};

exports.updateActive = async function (req, res) {
    const valid = await validateAuth(req, res)
    if(valid){
        Config.find({ Beamline: req.params.beamline}, async function (err, config) {
            if (err){
                const response = {
                    status: "error",
                    message: "Couldn't find record " + err
                };
                console.log(response)
                res.status(404).send(response);
            }
            // config is an array, therefore the first element is beamline record
            if(req.body.ActiveConfig in config[0].Configuration || req.body.ActiveConfig === "All"){
                config[0].ActiveConfig = req.body.ActiveConfig
                //save and check errors
                config[0].save(function (err) {
                    if (err){
                        const response = {
                            status: "error",
                            message: "Couldn't save record " + err
                        };
                        console.log(response)
                        res.status(500).send(response);
                    }
                    else{
                        const response = {
                            status: "success",
                            message: "Active configuration has been successfully updated",
                            data: config[0]
                        };
                        console.log(response)
                        res.send(response);
                    }
                });
            }else{
                const response = {
                    status: "error",
                    message: "No valid active configuration found"
                };
                console.log(response)
                res.status(404).send(response);
            }
        });
    }
};
