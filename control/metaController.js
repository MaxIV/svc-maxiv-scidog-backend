//Import MetaData Model
Meta = require('../model/metaModel');

const validateAuth = require('../utils/validateAuth');
const isObject = require('../utils/isObject');

//For index
exports.index = function (req, res) {
    Meta.get(function (err, meta) {
        if (err){
            const response = {
                status: "error",
                message: "Couldn't find records " + err
            };
            console.log(response)
            res.status(404).send(response);
        }
        else{
            const response = {
                status: "success",
                message: "Fetched metadata",
                data: meta
            };
            res.send(response);
        }
    });
};

//For creating new meta record
exports.add = async function (req, res) {
    const valid = await validateAuth(req, res)
    if(valid){
        var skip = false;
        var meta = new Meta();

        //populating meta fields
        req, res, skip = await validateForm(req, res)
        if(!skip){
            meta.Group = req.body.Group
            meta.Name = req.body.Name
            meta.Device = req.body.Device
            meta.Attribute = req.body.Attribute
            meta.Unit = req.body.Unit
            meta.Beamline = req.body.Beamline
            meta.Mapping = req.body.Mapping

            //Save and check error
            meta.save(function (err) {
                if (err){
                    const response = {
                        status: "error",
                        message: "Couldn't save record " + err
                    };
                    console.log(response)
                    res.status(500).send(response);
                }
                else{
                    const response = {
                        status: "success",
                        message: "Metadata has been successfully added",
                        data: meta
                    };
                    console.log(response)
                    res.send(response);
                }
            });
        }
    }
};

exports.addBulk = async function (req, res) {
    const valid = await validateAuth(req, res)

    if(!valid) {
        return
    }

    // Make sure that the body is a array of objects
    if(isObject(req.body)) {
        req.body = [req.body]
    }

    // Responder function since callback hell
    var responder = function(metas) {
        const response = {
                status: "success",
                message: "Metadata has been successfully added",
                data: metas
        };
        console.log(response)
        res.send(response);
    }

    var metas = []
    for (let i = 0; i < req.body.length; i++) {
        var record = req.body[i]

        // Validate the record to make sure we can continue with creating
        record = await sanitizeRecord(record)

        // Create meta object
        var meta = new Meta();
            meta.Group = record.Group
            meta.Name = record.Name
            meta.Device = record.Device
            meta.Attribute = record.Attribute
            meta.Unit = record.Unit
            meta.Beamline = record.Beamline
            meta.Mapping = record.Mapping

        // Don't re-create
        if(await metaExists(meta)) {
            continue
        }

        meta.save(function (err) {
            if (!err) {
                metas.push(meta)

                // Are we on the final one and all is good? Then respond to the user
                if (i == req.body.length-1) {
                    responder(metas)
                }
                return
            } else {
                const response = {
                    status: "error",
                    message: "Couldn't save record " + err,
                }
                console.log(response)
                res.status(500).send(response)
            }

        });
    }

    // If all meta records already existed, we still need to respond
    responder(metas)
}

// View MetaData
exports.view = function (req, res) {
    Meta.findById(req.params.meta_id, function (err, meta) {
        if (err){
            const response = {
                status: "error",
                message: "Couldn't find record " + err
            };
            console.log(response)
            res.status(404).send(response);
        }
        else{
            const response = {
                status: "success",
                message: 'Scientific metadata record',
                data: meta
            };
            res.send(response);
        }

    });
};

// View Specific MetaData
exports.viewBeamline = function (req, res) {
    Meta.find({ Beamline: req.params.beamline}, function (err, meta) {
        if (err){
            const response = {
                status: "error",
                message: "Couldn't find records " + err
            };
            console.log(response)
            res.status(404).send(response);
        }
        else{
            const response = {
                status: "success",
                message: 'Scientific metadata beamline records for ' + req.params.beamline,
                data: meta
            };
            res.send(response);

        }
    });
};

// Update MetaData
exports.update = async function (req, res) {
    const valid = await validateAuth(req, res)
    if(valid){
        let skip = false;
        Meta.findById(req.params.meta_id, async function (err, meta) {
            if (err){
                const response = {
                    status: "error",
                    message: "Couldn't find record " + err
                };
                console.log(response)
                res.status(404).send(response);
            }
            req, res, skip = await validateForm(req, res);
            if(!skip){
                meta.Group = req.body.Group
                meta.Name = req.body.Name
                meta.Device = req.body.Device
                meta.Attribute = req.body.Attribute
                meta.Unit = req.body.Unit
                meta.Beamline = req.body.Beamline
                meta.Mapping = req.body.Mapping

                //save and check errors
                meta.save(function (err) {
                    if (err){
                        const response = {
                            status: "error",
                            message: "Couldn't save record " + err
                        };
                        console.log(response)
                        res.status(500).send(response);
                    }
                    else{
                        const response = {
                            status: "success",
                            message: "Metadata has been successfully updated",
                            data: meta
                        };
                        console.log(response)
                        res.send(response);
                    }
                });
            }
        });
    }
};

// Delete Meta
exports.delete = async function (req, res) {
    const valid = await validateAuth(req, res)
    if(valid){
        Meta.deleteOne({
            _id: req.params.meta_id
        }, function (err) {
            if (err){
                const response = {
                    status: "error",
                    message: "Couldn't delete record " + err
                };
                console.log(response)
                res.status(500).send(response);
            }
            else{
                const response = {
                    status: "success",
                    message: 'Metadata has been successfully deleted'
                };
                console.log(response)
                res.send(response);
            }
        });
    }
};




//Helper functions
var validateForm = async function(req, res){
    var update = true;
    var skip = false;
    const metaSchemaNotRequired = ['Group', 'Unit', 'Mapping'];
    metaSchemaNotRequired.forEach(element => {
        if(!(element in req.body)){
            if (element == 'Mapping')
                req.body[element]= []
            else
                req.body[element] = ''
        }
    })

    //check if record exists
    const records = await Meta.find({ Name: req.body.Name, 'Device': req.body.Device, 'Attribute': req.body.Attribute, 'Beamline': req.body.Beamline})
    records.forEach(function(item){
        if(req.params.meta_id == item._id){
            update = false;
        }
    })

    if(records.length>=1 && update){
        skip = true;
        const response = {
            status: "error",
            message: "Entry for `Name`, `Device`, `Attribute` already exists for this beamline"
        };
        console.log(response)
        res.status(405).send(response);
    }

    return req, res, skip
}

//Helper functions
var sanitizeRecord = async function(record){
    var update = true;
    const metaSchemaNotRequired = ['Group', 'Unit', 'Mapping'];
    metaSchemaNotRequired.forEach(element => {
        if(!(element in record)){
            if (element == 'Mapping')
		record[element]= []
            else
                record[element] = ''
        }
    })

    return record
}

var metaExists = async function(meta) {
    const records = await Meta.find({ Name: meta.Name, Device: meta.Device, Attribute: meta.Attribute, Beamline: meta.Beamline })
    return records.length > 0
}
