var config = require('./config.json');
var isObject = require('./isObject.js');

var validateAuth = async function(req, res){
    var beamline = '',
    jwt = '',
    message = 'User authorization failed: Reason unknown',
    valid = false,
    cookies = ''

    try{
        cookies = req.headers.cookie;
        cookies.split(';').forEach(function( cookie ) {
            var parts = cookie.split('=');
            if(parts[0].trim() === 'jwt'){
                jwt = parts[1]
                return;
            }
        });

        if ("beamline" in req.params){
            beamline = req.params.beamline
        }
        else if ("meta_id" in req.params){
            const meta = await Meta.findById(req.params.meta_id)
            beamline = meta.Beamline
        }
        else if ("config_id" in req.params){
            const config = await Config.findById(req.params.config_id)
            beamline = config.Beamline
        }
	else if (isObject(req.body) == false) {
	    beamline = req.body[0].Beamline
	}
        else {
            beamline = req.body.Beamline
        }

        const fetch = require('node-fetch');
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({"jwt": jwt}),
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
        };

        await fetch(config.AuthURL, requestOptions)
        .then(response => response.json())
        .then(decoded => {
            if(decoded.groups.includes(beamline) ||
                decoded.groups.includes('KITS')){
                    valid = true
                    console.log(`"${ decoded.username }" successfully accessed the system.`)
                }
                else{
                    message = 'User not authorized to perform action: Not part of beamline group'
                }
        })
        .catch(error => {
            message = 'Authorization request failed to process: Cookie has no valid JWT'
            console.log('JWT auth error: ', error)
        });
    } catch(error){
        message = 'Cookie not found'
        console.log('Cookie error: ', error)
    }

    if(!valid){
        res.status(401)
        .send({
            status: "error",
            message: message
        });
    }

    return valid
}

module.exports = validateAuth
