var isObject = function(obj) {
    return typeof obj === 'object' &&
	!Array.isArray(obj) &&
	obj !== null &&
	obj !== undefined
}

module.exports = isObject
