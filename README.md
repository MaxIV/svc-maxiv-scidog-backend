# svc-maxiv-scidog-backend

## SciDog - Scientific (Meta)Data Organizer GUI
The SciDog is used to store and configure metadata that should be recorded and
stored in SciCat. Each beamline is responsible for their own set of metadata
that they wish to record. They can add/remove their metadata variables through
SciDog [https://scidog.maxiv.lu.se](https://scidog.maxiv.lu.se).

Each set of metadata consists of an optional group, a human-readable name for
the variable, the device and attribute combination and optionally a unit
corresponding to the value.

## Available Scripts

In the project directory, you can run:

```
docker-compose up
```

Runs the app in the development mode. Open
[localhost:7001/api](http://localhost:7001/api) to view it in the browser.

## Frontend

The frontend can be found
[here](https://gitlab.maxiv.lu.se/kits-maxiv/svc-maxiv-scidog-frontend/). To run
the frontend and backend together on your local machine, add a proxy to the
frontend package.json as follows: `"proxy":
"http://localhost:7001",`

## Application Endpoints

The application provides an interface to various interactions with the
database. These are accessed through the endpoints. Each endpoint has some
actions associated to them. All `POST`, `PUT`, `PATCH` and `DELETE` requests
require a cookie with `jwt` in the header. They are described as follows:

| Endpoint                       | Request     | Response/Input                                                                                                                       |
| -----------                    | ----------- | -----------                                                                                                                          |
| api/                           | GET         | Responds with JSON object containing status and message for the default page                                                         |
| api/meta                       | GET         | Responds with JSON object containing status, message and data for all saved meta records                                             |
| api/meta                       | POST        | Input JSON object containing Name, Device, Attribute, Beamline (mandatory) and Group, Unit and Mapping (optional)                    |
| api/meta-bulk                  | POST        | Input JSON list with object containing Name, Device, Attribute, Beamline (mandatory) and Group, Unit and Mapping (optional)          |
| api/meta/`meta_id`             | GET         | Responds with JSON object containing status, message and data for that `meta_id`                                                     |
| api/meta/`meta_id`             | PATCH/PUT   | Input JSON object containing Name, Device, Attribute, Beamline (mandatory) and Group, Unit and Mapping (optional) for that `meta_id` |
| api/meta/`meta_id`             | DELETE      | Deletes record for that `meta_id`                                                                                                    |
| api/beamline/`beamline`        | GET         | Responds with JSON object containing status, message and data for all saved meta records for that `beamline`                         |
| api/config                     | GET         | Responds with JSON object containing status, message and data for all saved configuration records                                    |
| api/config                     | POST        | Input JSON object containing Beamline, Configuration, ActiveConfig (mandatory) - internal use only                                   |
| api/config/beamline/`beamline` | GET         | Responds with JSON object containing status, message and data with saved configuration record for that `beamline`                    |
| api/config/beamline/`beamline` | PUT/PATCH   | Input JSON object containing Configuration for that `beamline`                                                                       |
| api/config/active/`beamline`   | GET         | Responds with JSON object containing status, message and data with active meta records for that `beamline`                           |
| api/config/active/`beamline`   | PUT/PATCH   | Input JSON object containing ActiveConfig for that `beamline`                                                                        |

## Deployment

It is automatically deployed in k8s through the gitlab-ci. Please update the
version in the package.json. A [develop version](https://scidog-test-develop.apps.okdev.maxiv.lu.se/)
will be created when pushing to the develop branch. After updating the master branch,
add a tag with the current version to deploy to production.

### Configuration Variables

The configuration of the project is stored in `./utils/config.json`. These
settings help to set up the services that the app then utilizes. They should not
be changed.
- AppPort : The port on which the application will run
- MongoDBPort : The port on which the MongoDB instance will run
- AuthURL : The auth service from MAXIV that authorizes users and returns their user group permissions
