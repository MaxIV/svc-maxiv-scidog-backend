
var mongoose = require('mongoose');
var express = require('express');
var nocache = require('nocache');
var config = require('../utils/config.json');
require('log-timestamp');

var app = express();
// adding no cache to headers 
app.use(nocache());

//import routes
var apiRoutes = require("../router/routes");

//configure urlencoded to hande the post requests
app.use(express.urlencoded({
    extended: true
}));
app.use(express.json());

//connect to mongoose
var dbHost = process.env.MONGODB_HOSTNAME;
if (dbHost === null){
  throw new Error('database host missing in environment');
}
var dbName = process.env.MONGODB_DBNAME;
if (dbName === null){
  throw new Error('database name missing in environment');
}

const dbPath = `mongodb://${dbHost}:${config.MongoDBPort}/${dbName}`;

const username = process.env.MONGODB_DBUSERNAME
if (username === null){
  throw new Error('database username missing in environment');
}

const password = process.env.MONGODB_DBPASSWORD
if (password === null){
  throw new Error('database password missing in environment');
}


const options = {useNewUrlParser: true, useUnifiedTopology: true, user: username, pass: password}
mongoose.connect(dbPath, options);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'CONNECTION ERROR: Failed to connect to `' + dbPath + '`\n\n'));
db.once('open', function() {
  console.log('Connected');
})
.then(
  () => {
    // server port, not to be changed
  var port = config.AppPort;


  // //use API routes in the app
  app.use('/api', apiRoutes.router)

  // launch app to the specified port
  app.listen(port, function() {
      console.log("Running SciDog backend on port "+ port);
  });
  }
)
