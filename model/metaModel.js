//metaModel.js
const mongoose = require('mongoose')
let Schema = mongoose.Schema
//schema
var metaSchema = Schema({
    Beamline: {
        type: String,
        required: true
    },
    Group: {
        type: String
    },
    Name: {
        type: String,
        required: true
    },
    Device: {
        type: String,
        required: true
    },
    Attribute: {
        type: String,
        required: true
    },
    Unit: {
        type: String
    },
    Mapping:{
        type: Array
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});


// Export Meta Model
var Meta = module.exports = mongoose.model('meta', metaSchema);

module.exports.get = function (callback, limit) {
    Meta.find(callback).limit(limit);

}
