//configModel.js
const mongoose = require('mongoose')
let Schema = mongoose.Schema
//schema
var configSchema = Schema({
    Beamline: {
        type: String,
        required: true
    },
    Configuration: {
        type: Object,
        default: {},
        required: true
    },
    ActiveConfig: {
        type: String,
        default: "All",
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now
    }
},
{ minimize: false });


// Export Meta Model
var Config = module.exports = mongoose.model('config', configSchema);

module.exports.get = function (callback, limit) {
    Config.find(callback).limit(limit);

}
