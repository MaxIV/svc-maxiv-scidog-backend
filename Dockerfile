FROM harbor.maxiv.lu.se/dockerhub/library/node:15.14.0-buster-slim
ENV NODE_ENV=production
WORKDIR /app
COPY ["package.json", "./"]
RUN npm install --no-cache --production --silent && mv node_modules ../
COPY . .
EXPOSE 7001

# Production
CMD ["npm", "start"]

 # run in debug mode to get file watcher that restart the server on changes
# CMD ["npm", "start", "debug"]
